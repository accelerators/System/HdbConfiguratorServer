# Project HdbConfiguratorServer

Maven Java project

This class is able to use HdbConfigurator classes to
Add/Start/Stop archiving attributes on HDB++ subscribers.
It is mainly used to do it from device servers.


## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/System/HdbConfiguratorServer
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* hdbpp-configurator.jar
* JTango.jar
  

#### Toolchain Dependencies 

* javac 7 or higher
* maven
  

### Build Flags 

Custom build flags for the project device server: 

| Flag | Default | Use |
|------|---------|-----|
|FLAG|STATE|DESCRIPTION|

### Build


Instructions on building the project.

```
cd HdbConfiguratorServer
mvn package
```

