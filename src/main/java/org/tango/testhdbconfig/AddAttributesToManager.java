//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.testhdbconfig;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is able to
 *
 * @author verdier
 */

public class AddAttributesToManager {
    private List<DeviceProxy> deviceProxies = new ArrayList<>();
    private List<AddThread> threads = new ArrayList<>();

    private static final int NB_MAN = 4;
    private static final String[] attributes = {
            "tango://acs.esrf.fr:10000/srvac/v-ip/c02-ch09-1/pressure",
            "tango://acs.esrf.fr:10000/srvac/v-ip/c22-ch09-1/pressure",
            "tango://acs.esrf.fr:10000/srvac/v-ip/c23-ch09-1/pressure",
            "tango://acs.esrf.fr:10000/srvac/v-ip/c24-ch09-1/pressure",
    };
    //===============================================================
    //===============================================================
    public AddAttributesToManager(String deviceName) throws DevFailed {
        for (int i=0 ; i<NB_MAN ;i++) {
            deviceProxies.add(new DeviceProxy(deviceName+(1+i)));
            threads.add(new AddThread(i));
        }
    }
    //===============================================================
    //===============================================================
    public void addAttributes() {
        long t0 = System.currentTimeMillis();
        for (int i=0 ; i<NB_MAN ;i++) {
            threads.get(i).start();
        }
        for (int i=0 ; i<NB_MAN ;i++) {
            try {
                threads.get(i).join();
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        long t1 = System.currentTimeMillis();
        System.out.println("elapsed time : " + (t1 - t0) + " ms");
    }
    //===============================================================
    //===============================================================
    public void displayStatus() throws DevFailed {
        System.out.println("-----------------------------------");
        for (DeviceProxy deviceProxy : deviceProxies)
            System.out.println(deviceProxy.name() + ": " + deviceProxy.status());
        System.out.println("-----------------------------------");
    }
    //===============================================================
    //===============================================================
    public void displayAddingStatus() throws DevFailed {
        System.out.println("-----------------------------------");
        for (int i=0 ; i<NB_MAN ; i++) {
            DeviceData in = new DeviceData();
            in.insert(threads.get(i).id);
            DeviceData out = deviceProxies.get(i).command_inout("AddingStatus", in);
            System.out.println(deviceProxies.get(i).name() + ": " + out.extractString()+"\n");
        }
        System.out.println("-----------------------------------");
    }
    //===============================================================
    //===============================================================
    private void removeAttributes() throws DevFailed {
        for (int i = 0; i < NB_MAN; i++) {
            DeviceData in = new DeviceData();
            String[] array = { attributes[i] };
            in.insert(array);
            System.out.println("Removing " + attributes[i]);
            deviceProxies.get(i).command_inout("RemoveAttributes", in);
        }
    }
    //===============================================================
    //===============================================================
    private class AddThread extends Thread {
        private DeviceProxy proxy;
        private DeviceData  argIn;
        private int id=-1;
        //===========================================================
        private AddThread(int num) throws DevFailed {
            this.proxy = deviceProxies.get(num);
            String[] array = {
                    "ES-PV"+(num+1),
                    attributes[num],
                    "true",
            };
            argIn = new DeviceData();
            argIn.insert(array);
        }
        //===========================================================
        @Override
        public void run() {
            try {
                DeviceData argOut = proxy.command_inout("AddAttributes", argIn);
                id = argOut.extractLong();
            } catch (DevFailed e) {
                System.err.println(proxy.name() + ":  " + e.errors[0].desc);
            }
        }
        //===========================================================
    }
    //===============================================================
    //===============================================================

    //===============================================================
    //===============================================================
    public static void main(String[] args) {

        String deviceName = "pv/hdb-config/test";

        if (args.length > 0)
            deviceName = args[0];
        try {
            AddAttributesToManager client = new AddAttributesToManager(deviceName);
            client.displayStatus();
            client.addAttributes();
            Thread.sleep(12000);
            client.displayAddingStatus();
            Thread.sleep(8000);
            client.removeAttributes();
        } catch (DevFailed e) {
            Except.print_exception(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
