//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbconfiguratorserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;


/**
 * This class is a set static methods
 *
 * @author verdier
 */

public class Utils {
    private static final boolean trace = (System.getenv("TRACE")!=null && System.getenv("TRACE").equalsIgnoreCase("true"));
    //===============================================================
    //===============================================================
    public static boolean attributeIsStarted(String attributeName, String subscriberName) throws DevFailed {
        return attributeIsStarted(attributeName, new DeviceProxy(subscriberName));
    }
    //===============================================================
    //===============================================================
    public static boolean attributeIsStarted(String attributeName, DeviceProxy subscriber) throws DevFailed {
        //	Read started list
        DeviceAttribute attribute = subscriber.read_attribute("AttributeStartedList");
        String[] startedAttributes = attribute.extractStringArray();
        //	Check if attribute in list
        for (String startedAttribute : startedAttributes) {
            if (startedAttribute.equalsIgnoreCase(attributeName))
                return true;
        }
        return false;
    }
    //===============================================================
    //===============================================================
    public static String getArchiver(String attributeName, DeviceProxy configuratorProxy) throws DevFailed {
        DeviceData argIn = new DeviceData();
        argIn.insert(attributeName);
        DeviceData argOut = configuratorProxy.command_inout("AttributeGetArchiver", argIn);
        return argOut.extractString();
    }
    //===============================================================
    //===============================================================
    public static void startAttribute(String attributeName, DeviceProxy managerProxy) throws DevFailed {
        DeviceData argIn = new DeviceData();
        argIn.insert(attributeName);
        managerProxy.command_inout("AttributeStart", argIn);
    }
    //===============================================================
    //===============================================================
    public static void traceExecution(String str) {
        if (trace) System.out.println(str);
    }
    //===============================================================
    //===============================================================
}
