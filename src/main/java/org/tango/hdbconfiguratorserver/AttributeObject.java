//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2017,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbconfiguratorserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.Except;
import org.tango.hdb_configurator.common.Context;
import org.tango.hdb_configurator.common.Strategy;

import java.util.StringTokenizer;


/**
 * This class model an attribute to be added
 * with it config (strategy, TTL, ...)
 *
 * @author verdier
 */

public class AttributeObject {
    private String name;
    private Strategy strategy;
    private long ttl=0;
    //===============================================================
    //===============================================================
    public AttributeObject(String line, Strategy defaultStrategy) throws DevFailed {
        StringTokenizer stk = new StringTokenizer(line, ";");
        if (stk.hasMoreElements()) {
            name = stk.nextToken().trim();
        }
        if (stk.hasMoreElements()) {
            Context context = new Context(getStrategyName(stk.nextToken()), true, "");
            this.strategy = new Strategy();
            strategy.add(context);
            if (stk.hasMoreElements()) {
                ttl = getTTL(stk.nextToken());
            }
        }
        else
            strategy = defaultStrategy;
    }
    //===============================================================
    //===============================================================
    private String getStrategyName(String str) throws DevFailed {
        String expected = "strategy=";
        if (!str.toLowerCase().startsWith(expected))
            Except.throw_exception("SyntaxError", "Strategy bad syntax: " + str);
        return str.substring(expected.length());
    }
    //===============================================================
    //===============================================================
    private long getTTL(String str) throws DevFailed {
        String expected = "ttl=";
        if (!str.toLowerCase().startsWith(expected))
            Except.throw_exception("SyntaxError", "TTL bad syntax: " + str);
        return Long.parseLong(str.substring(expected.length()));
    }
    //===============================================================
    //===============================================================
    public String getName() {
        return name;
    }
    //===============================================================
    //===============================================================
    public Strategy getStrategy() {
        return strategy;
    }
    //===============================================================
    //===============================================================
    public long getTTL() {
        return ttl;
    }
    //===============================================================
    //===============================================================
    public String toString() {
        return name + ":  " + strategy + " (" + ttl + ")";
    }
    //===============================================================
    //===============================================================
}
