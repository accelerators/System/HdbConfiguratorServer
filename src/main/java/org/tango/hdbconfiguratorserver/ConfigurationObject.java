//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbconfiguratorserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import org.tango.hdb_configurator.common.Strategy;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is a set of information for one configuration
 *
 * @author verdier
 */

public class ConfigurationObject {
    private long creationTime = System.currentTimeMillis();
    private String subscriberName;
    private List<String> attributeNames;
    private List<AttributeObject> attributeToAdd = new ArrayList<>();
    private List<AttributeObject> attributeToStart = new ArrayList<>();
    private Strategy defaultStrategy;
    private boolean pushedByCode;
    private int confID;
    private String error = null;
    //===============================================================
    //===============================================================
    public ConfigurationObject(int confID,
                               String subscriberName,
                               List<String> attributeNames,
                               Strategy defaultStrategy,
                               boolean pushedByCode) {
        this.confID = confID;
        this.subscriberName = subscriberName;
        this.attributeNames = attributeNames;
        this.defaultStrategy = defaultStrategy;
        this.pushedByCode = pushedByCode;
    }
    //===============================================================
    //===============================================================
    public boolean initializeAttributes(DeviceProxy configuratorProxy) {
        try {
            for (String attributeName : attributeNames) {
                //  Check if already managed
                String archiverName = Utils.getArchiver(attributeName, configuratorProxy);
                if (archiverName!=null && !archiverName.isEmpty()) {
                    if (!Utils.attributeIsStarted(attributeName, archiverName))
                        attributeToStart.add(
                                new AttributeObject(attributeName, defaultStrategy));
                }
                else {
                    //  Not already managed --> to be added
                    attributeToAdd.add(new AttributeObject(attributeName, defaultStrategy));
                }
            }
            return true;
        }
        catch (DevFailed e) {
            System.err.println(e.errors[0].desc);
            error = e.errors[0].desc;
            return false;
        }
    }
    //===============================================================
    //===============================================================
    public int getConfID() {
        return confID;
    }
    //===============================================================
    //===============================================================
    public long getCreationTime() {
        return creationTime;
    }
    //===============================================================
    //===============================================================
    public String getSubscriberName() {
        return subscriberName;
    }
    //===============================================================
    //===============================================================
    public List<AttributeObject> getAttributeToAdd() {
        return attributeToAdd;
    }
    //===============================================================
    //===============================================================
    public List<AttributeObject> getAttributeToStart() {
        return attributeToStart;
    }
    //===============================================================
    //===============================================================
    public boolean isPushedByCode() {
        return pushedByCode;
    }
    //===============================================================
    //===============================================================
    public boolean hasFailed() {
        return error!=null;
    }
    //===============================================================
    //===============================================================
    public void setError(String error) {
        this.error = error;
        System.err.println(error);
    }
    //===============================================================
    //===============================================================
    public String getError() {
        return error;
    }
    //===============================================================
    //===============================================================
    public String toString() {
        return "ID=" + confID + " - " + attributeNames.size() + " Attributes";
    }
    //===============================================================
    //===============================================================
}
