//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package org.tango.hdbconfiguratorserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import org.tango.hdb_configurator.common.HdbAttribute;
import org.tango.hdb_configurator.configurator.ManageAttributes;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is a list of configuration object
 * and manage thread in charge to execute
 * serialized configurations
 *
 * @author verdier
 */

public class ConfigurationExecution  {
    private DeviceProxy managerProxy;
    private long waitBeforeStart;
    private ConfigurationThread thread;
    private List<ConfigurationObject> configurationToDoList = new ArrayList<>();
    private List<ConfigurationObject> configurationDoneList = new ArrayList<>();
    private final Object monitor = new Object();
    private static final long CONFIG_AGE_MAX = 5*60*1000;
    //===============================================================
    //===============================================================
    public ConfigurationExecution(DeviceProxy managerProxy, long waitBeforeStart) {
        this.managerProxy = managerProxy;
        this.waitBeforeStart = waitBeforeStart;
        thread = new ConfigurationThread();
        thread.start();
    }
    //===============================================================
    //===============================================================
    public void addConfiguration(ConfigurationObject configurationObject) {
        synchronized (monitor) {
            configurationToDoList.add(configurationObject);
            Utils.traceExecution("addConfiguration()");
            thread.wakeUp();
        }
    }
    //===============================================================
    //===============================================================
    private ConfigurationObject getConfiguration(int index, List<ConfigurationObject> list) {
        synchronized (monitor) {
            return list.get(index);
        }
    }
    //===============================================================
    //===============================================================
    private void removeFirst(List<ConfigurationObject> list) {
        synchronized (monitor) {
            list.remove(0);
        }
    }
    //===============================================================
    //===============================================================
    public String getAddingError(int id) {
        //  Check in Done list
        String error = "No Error";
        for (int i=0 ; i<configurationDoneList.size() ; i++) {
            ConfigurationObject configuration = getConfiguration(i, configurationDoneList);
            if (configuration.getConfID()==id) {
                if (configuration.hasFailed())
                    error = configuration.getError();
            }
        }
        return error;
    }
    //===============================================================
    //===============================================================
    public DevState addingState(int id) throws DevFailed {
        //  Check if ToDo list
        for (int i=0 ; i<configurationToDoList.size() ; i++) {
            if (getConfiguration(i, configurationToDoList).getConfID() == id)
                return DevState.MOVING;
        }

        //  Check in Done list
        for (int i=0 ; i<configurationDoneList.size() ; i++) {
            ConfigurationObject configuration = getConfiguration(i, configurationDoneList);
            if (configuration.getConfID()==id) {
                if (configuration.hasFailed())
                    return DevState.ALARM;
                else
                    return DevState.ON;
            }
        }
        System.err.println("-------> ID=" + id + " not found in list");
        Except.throw_exception("IdNotFound", "ID=" + id + " not found in list");
        return null; // never occur after throwing DevFailed
    }
    //===============================================================
    //===============================================================






    //===============================================================
    /**
     * A thread to manage execution
     */
    //===============================================================
    private class ConfigurationThread extends Thread {
        private boolean runThread = true;

        //===========================================================
        public void run() {
            while (runThread) {
                Utils.traceExecution("Thread running");
                while (!configurationToDoList.isEmpty()) {
                    //  Do execution and move object from ToDo to Done list
                    ConfigurationObject configuration = getConfiguration(0, configurationToDoList);
                    doExecution(configuration);
                    configurationDoneList.add(configuration);
                    removeFirst(configurationToDoList);
                }
                runMyGarbageCollector();
                nextLoop();
            }
        }
        //===========================================================
        private void runMyGarbageCollector() {
            //  Check if ConfigurationObjects done are so old to be removed
            int nbToRemove = 0;
            for (ConfigurationObject configuration : configurationDoneList) {
                long age = System.currentTimeMillis() - configuration.getCreationTime();
                if (age>=CONFIG_AGE_MAX)
                    nbToRemove++;
                else
                    break; // after they are younger
            }
            for (int i=0 ; i<nbToRemove ; i++) {
                Utils.traceExecution("removing idx=" + i);
                removeFirst(configurationDoneList);
            }
        }
        //===========================================================
        private void doExecution(ConfigurationObject configuration) {
            Utils.traceExecution("doExecution for id = " + configuration.getConfID());
            waitBeforeStart(configuration);
            Utils.traceExecution("check configuration");
            if (configuration.initializeAttributes(managerProxy)) {
                Utils.traceExecution("Do the configuration");
                addAttributes(configuration);
                startArchiving(configuration);
                Utils.traceExecution("Configuration done for ID=" + configuration.getConfID());
            }
        }
        //===========================================================
        private void addAttributes(ConfigurationObject configuration) {
            try {
                // Create a hdb attribute list from configuration
                List<HdbAttribute> hdbAttributes = new ArrayList<>();
                List<AttributeObject> attributeList = configuration.getAttributeToAdd();
                boolean pushedByCode = configuration.isPushedByCode();
                for (AttributeObject attributeObject : attributeList) {
                    Utils.traceExecution(attributeObject.toString());
                    HdbAttribute hdbAttribute = new HdbAttribute(
                            attributeObject.getName(), attributeObject.getStrategy(), pushedByCode);
                    if (attributeObject.getTTL()>0)
                        hdbAttribute.setTTL(attributeObject.getTTL());
                    hdbAttributes.add(hdbAttribute);

                }
                ManageAttributes.addAttributes(managerProxy,
                        configuration.getSubscriberName(), hdbAttributes);
            }
            catch (DevFailed e) {
                String error = e.errors[0].desc;
                if (error.contains("already archived by")) // Not an error
                    System.out.println(error);
                else
                    configuration.setError(managerProxy.name()+": "+e.errors[0].desc);
            }
        }
        //===========================================================
        private void startArchiving(ConfigurationObject configuration) {
            List<AttributeObject> attributeList = configuration.getAttributeToStart();
            for (AttributeObject attributeObject : attributeList){
                try {
                    Utils.startAttribute(attributeObject.getName(), managerProxy);
                }
                catch (DevFailed e) {
                    String error = e.errors[0].desc;
                    if (error.contains("already archived by")) // Not an error
                        System.out.println(error);
                    else
                        configuration.setError(error);
                }
            }
        }
        //===========================================================
        private void waitBeforeStart(ConfigurationObject configuration) {
            long timeToWait = waitBeforeStart -
                    (System.currentTimeMillis() - configuration.getCreationTime());
            //  Check if must wait a bit ?
            if (timeToWait>0) {
                Utils.traceExecution("Sleeping " + timeToWait + " ms");
                try { sleep(timeToWait); } catch (InterruptedException e) { /* */ }
            }
            else
                Utils.traceExecution("No sleep (" + timeToWait + ")");
        }
        //===========================================================
        private synchronized void nextLoop() {
            try { wait(CONFIG_AGE_MAX); } catch (InterruptedException e) { /* */ }
        }
        //===========================================================
        private synchronized void wakeUp() {
            notify();
        }
        //===========================================================
    }
    //===============================================================
    //===============================================================
}
