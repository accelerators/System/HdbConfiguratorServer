# Changelog

#### HdbConfiguratorServer-1.8 - 03/06/20:
    It is now able to manage several devices in one server

#### HdbConfiguratorServer-1.7 - 09/08/18:
    Implement Strategy/TTL for each added attribute
    Compatibility with HdbConfigurator 3.0 and higher

#### HdbConfiguratorServer-1.5 - 28/08/17:
    Manage strategy (default contexts) when adding attributes

#### HdbConfiguratorServer-1.4 - 22/03/17:
    Checked with multiple TANGO_HOST

#### HdbConfiguratorServer-1.3 - 08/02/17:
    Compatibility with HdbConfigurator 2.0 and higher

#### HdbConfiguratorServer-1.2 - 06/06/16:
    RemoveAttributes command added.

#### HdbConfiguratorServer-1.1 - 27/05/16:
    A thread to execute the subscriptions later added.

#### HdbConfiguratorServer-1.0 - 13/05/16:
    Initial revision
